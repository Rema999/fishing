import React from 'react';
import { Route, Switch } from 'react-router-dom';

const Routers = () => {
  return (
    <Switch>
      <Route path='/registration' render={ () => <div>There will be registration</div>}/>
      <Route path='/Shoping-cart' render={ () => <div>There will be Shoping cart</div>}/>
      <Route path='/checkout' render={ () => <div>There will be checkout</div>}/>
    </Switch>
  );
};

export default Routers;
