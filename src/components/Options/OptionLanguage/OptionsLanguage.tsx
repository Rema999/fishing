import React, { useState } from 'react';
import i18n from '../../../i18n/Translation';
import clsx from 'clsx';
import classes from '../Options.module.scss';

interface IOptionsLanguage {
  handleDropDawn: () => void
}

const OptionsLanguage: React.FC<IOptionsLanguage> = ({ handleDropDawn }) => {

  const lngs: any = {
    en: { nativeName: 'English-1' },
    ru: { nativeName: 'Russian-2' },
    ua: { nativeName: 'Ukrainian-3' },
  };
  const [toggleClass, setToggleClass] = useState<null | number>(null);
  const handletoggleClass = (index: number) => {
    setToggleClass(index);
  };

  return (
    <div className={clsx(classes.dropdown)}>
      {Object.keys(lngs).map((lng, index) => (
        <button key={lng} type="submit"
          onClick={() => {
            handleDropDawn();
            i18n.changeLanguage(lng);
            handletoggleClass(index);
          }}
          className={clsx({ [classes.classActive]: toggleClass === index })}
        >
          {lngs[lng].nativeName}
        </button>
      ))}
    </div>

  );

};

export default OptionsLanguage;
