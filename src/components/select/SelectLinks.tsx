import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import classes from './Select.module.scss';
import clsx from 'clsx';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';


interface ISelect {
  toggleSelect: boolean,
  handleDropDawn: () => void,
  handleClickAway: () => void,
  title: string,
}

const SelectLinks: React.FC<ISelect> = ({ toggleSelect, handleClickAway, title, handleDropDawn, children }) => {

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <div className={classes.selectWrapper} >
        <div className={classes.titleWrapper} onClick={handleDropDawn}>
          <span >{title}</span>
          <ExpandMoreIcon className={clsx(classes.initialIcon, { [classes.icon]: toggleSelect })}/>
        </div>
        <div className={clsx(classes.dropdownWrapper, { toggleElement: toggleSelect })}>
          { children }
        </div>
      </div>
    </ClickAwayListener>
  );
};

export default SelectLinks;
