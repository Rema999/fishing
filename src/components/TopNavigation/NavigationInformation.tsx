import React from 'react';
import { PurePhone } from '../../Tools/PurePhone';
import classes from './topNavigation.module.scss';

interface INavigationInformation {
  title: string,
  firstPhone?: string,
  secondPhone?: string
}

const NavigationInformation: React.FC<INavigationInformation> = ({ secondPhone, firstPhone, title, children }) => {

  return (
    <div className={classes.NavInfoWrapper}>
      {children}
      {title}
      {firstPhone && <a href={`callto:+${PurePhone(firstPhone)}`} >{firstPhone}</a>}
      {secondPhone && <a href={`callto:+${PurePhone(secondPhone)}`}>{secondPhone}</a>}
    </div>
  )
  ;
};

export default NavigationInformation;
