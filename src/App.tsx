import TopNavigation from './components/TopNavigation/TopNavigation';
import Routers from './components/Routes/Routes';
import   './styles/index.scss';
// import App222 from './Tools/testing';

function App() {
  return (
    <div>
      <Routers/>
      <TopNavigation />
      {/*<App222/>*/}
    </div>
  );
}

export default App;
